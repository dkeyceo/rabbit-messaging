package com.dkey.cloud.messaging.repositories;

import com.dkey.cloud.messaging.model.SimpleMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository  extends JpaRepository<SimpleMessage, Long> {

}
