package com.dkey.cloud.messaging.services;

import com.dkey.cloud.messaging.config.RabbitConfig;
import com.dkey.cloud.messaging.model.SimpleMessage;
import com.dkey.cloud.messaging.repositories.MessageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageService {

    private RabbitTemplate rabbitTemplate;
    private MessageRepository messageRepository;

    private static final Logger log = LoggerFactory.getLogger(MessageService.class);

    @Autowired
    public MessageService(RabbitTemplate rabbitTemplate, MessageRepository messageRepository) {
        this.rabbitTemplate = rabbitTemplate;
        this.messageRepository = messageRepository;
    }

    public void save(SimpleMessage simpleMessage){
        messageRepository.save(simpleMessage);
    }
    public SimpleMessage emit(SimpleMessage message){
        log.info("Emited message! -> "+ message.toString());
        SimpleMessage processedMessage = (SimpleMessage) rabbitTemplate.convertSendAndReceive(RabbitConfig.DEFAULT_PARSING_QUEUE, message);
        log.info("Already processed message! -> "+ processedMessage.toString());
        return processedMessage;
    }
    @RabbitListener(queues = RabbitConfig.DEFAULT_PARSING_QUEUE)
    public SimpleMessage consume(SimpleMessage message){
        log.info("Consumed message -> "+message.toString());
        message.setStatus("1");
        log.info("changed status message "+message);
        return message;
    }
    public SimpleMessage send(SimpleMessage message){
        save(message);
        SimpleMessage mes = emit(message);
        return mes;
    }
}
