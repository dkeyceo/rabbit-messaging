package com.dkey.cloud.messaging.controllers;

import com.dkey.cloud.messaging.model.SimpleMessage;
import com.dkey.cloud.messaging.services.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageController {
    private MessageService messageService;


    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @RequestMapping("/send")
    public ResponseEntity send(@RequestBody SimpleMessage message){
        SimpleMessage processedMessage = messageService.send(message);
        return ResponseEntity.status(HttpStatus.OK).body(processedMessage);
    }
}
